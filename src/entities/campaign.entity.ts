import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class Campaign extends Document {
    @Prop({ required: true })
    conversionType: string;

    @Prop({ required: true })
    countryTargeting: string;

    @Prop({ default: Date.now })
    updatedAt: Date;

    @Prop({ default: Date.now })
    createdAt: Date;
}

export const CampaignSchema = SchemaFactory.createForClass(Campaign);
