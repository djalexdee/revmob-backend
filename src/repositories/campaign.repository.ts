import { InternalServerErrorException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ResponseDto } from '../dto/response.dto';
import { Campaign } from '../entities/campaign.entity';
import { CreateCampaignDto } from '../modules/campaign/dto/createCampaign.dto';

export class CampaignRepository {
    constructor(
        @InjectModel(Campaign.name)
        private readonly campaignModel: Model<Campaign>,
    ) {}

    async createCampaign(createCampaignDto: CreateCampaignDto) {
        let campaign = new this.campaignModel(createCampaignDto);
        return campaign.save();
    }

    async getCampaigns() {
        let campaigns;
        try {
                campaigns = await this.campaignModel.find()
            let response: ResponseDto;

            if (campaigns.length > 0) {
                response = {
                    ok: true,
                    data: campaigns,
                    message: 'Get Campaigns Ok!',
                };
            } else {
                response = {
                    ok: true,
                    data: [],
                    message: 'Empty Campaigns',
                };
            }
            return response;
        } catch (error) {
            throw new InternalServerErrorException('Internal error when querying campaigns', error);
        }
    }

    async getCampaignByCountry(countryTargeting: string): Promise<Campaign> {
        let campaign;

        try {
            campaign = await this.campaignModel.find({ countryTargeting });
        } catch (error) {
            throw new InternalServerErrorException('Error connecting to MongoDB', error);
        }

        return campaign;
    }
}
