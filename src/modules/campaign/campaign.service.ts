import { Injectable } from '@nestjs/common';
import { CampaignRepository } from '../../repositories/campaign.repository';
import { CreateCampaignDto } from './dto/createCampaign.dto';

@Injectable()
export class CampaignService {
    constructor(private readonly campaignRepository: CampaignRepository) {}

    async createCampaign(createCampaignDto: CreateCampaignDto) {
        return this.campaignRepository.createCampaign(createCampaignDto);
    }

    async getCampaigns() {
        return this.campaignRepository.getCampaigns();
    }

    async getCampaignByCountry(countryTargeting: string) {
        return this.campaignRepository.getCampaignByCountry(countryTargeting);
    }
}
