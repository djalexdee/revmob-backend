import { IsNotEmpty, IsString } from 'class-validator';

export class CreateCampaignDto {
    @IsString()
    @IsNotEmpty()
    conversionType: string;

    @IsString()
    @IsNotEmpty()
    countryTargeting: string;
}
