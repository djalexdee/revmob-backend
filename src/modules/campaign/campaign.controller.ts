import { BadRequestException, Body, Controller, Get, HttpStatus, Param, Post, Res } from '@nestjs/common';
import { InjectConnection } from '@nestjs/mongoose';
import { Response } from 'express';
import { Connection } from 'mongoose';
import { CampaignService } from './campaign.service';
import { CreateCampaignDto } from './dto/createCampaign.dto';

@Controller('campaigns')
export class CampaignController {
    constructor(@InjectConnection() private readonly mongoConnection: Connection, private campaignService: CampaignService) {}

    @Post('/createCampaign')
    async createCampaign(@Body() createCampaignDto: CreateCampaignDto, @Res() res: Response) {
        const session = await this.mongoConnection.startSession();
        session.startTransaction();
        try {
            const newCampaign = await this.campaignService.createCampaign(createCampaignDto);
            await session.commitTransaction();
            return res.status(HttpStatus.CREATED).send(newCampaign);
        } catch (error) {
            await session.abortTransaction();
            throw new BadRequestException(error);
        } finally {
            session.endSession();
        }
    }

    @Get('/getCampaigns')
    async getCampaigns(@Res() res: Response) {
        const campaigns: any = await this.campaignService.getCampaigns();
        return res.status(HttpStatus.OK).send(campaigns);
    }

    @Get('/getCampaignByCountry/:countryTargeting')
    async getCampaignById(@Param('countryTargeting') countryTargeting: string, @Res() res: Response) {
        const campaign: any = await this.campaignService.getCampaignByCountry(countryTargeting);
        return res.status(HttpStatus.OK).send(campaign);
    }
}
