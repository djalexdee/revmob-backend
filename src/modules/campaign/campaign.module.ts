import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Campaign, CampaignSchema } from '../../entities/campaign.entity';
import { CampaignRepository } from '../../repositories/campaign.repository';
import { CampaignController } from './campaign.controller';
import { CampaignService } from './campaign.service';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: Campaign.name, schema: CampaignSchema }]),
    ],
    controllers: [CampaignController],
    providers: [CampaignService, CampaignRepository],
    exports: [CampaignService, CampaignRepository],
})
export class CampaignModule {}
